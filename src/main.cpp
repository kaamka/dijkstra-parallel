#include <iostream>
#include <mpi.h>
#include <fstream>

using namespace std;

// this can be implemented using dynamic allocation
const int SIZE = 300;    // maximum size of the graph
int G[SIZE][SIZE];         // adjacency matrix
bool visited[SIZE];        // nodes already visited
int D[SIZE];               // distance, cost to reach the node from start point
int pred[SIZE];            // predecessors we came to this node from
const int INF = 9999; // big enough number, bigger than any possible pred

int nproc, id;
MPI_Status status;
int N;  // NUMBER OF VERTEX


// structure used to exchange information between processes
typedef struct Vertex {
    int index;
    int cost;
} Vertex;


void readGraphMatrix()
{
    int vertices;    

    ifstream myfile;
    myfile.open ("../input/1.in");

    myfile >> vertices;

    for (int i = 0; i < vertices; i++) {
        for(int j = 0; j < vertices; j++) {
            myfile >> G[i][j];
            if (G[i][j] == -1) G[i][j] = INF;
        }

    }

    N = vertices;
}



// wysylanie do glownego procesu kawałków sciezki obliczonych na innych procesach
void gatherPredecessors() {

    // dismiss in case of single core
    if (nproc == 1) {
        return;
    }


    if (id == 0) {
        // If I am master node, I want to receive data certain number of times

        // Count how many predecessors I need to receive
        int count = 0;
        for (int i = id; i < N; i += nproc){
            count++;
        }

        // Receive and save to pred array
        Vertex tmp_pred;
        for(int i=0; i < N - count;  i++) {
            MPI_Recv(&tmp_pred, 1, MPI_2INT, MPI_ANY_SOURCE, 1, MPI_COMM_WORLD, &status);
            pred[tmp_pred.index] = tmp_pred.cost; //cost to poprzednik
        }

    } else {
        // If I am other node, I want to send my predecessors to master 
        Vertex predec;
        for (int i = id; i < N; i += nproc) {
            predec.index = i;
            predec.cost = pred[i]; // really predec not cost
            MPI_Send(&predec, 1, MPI_2INT, 0, 1, MPI_COMM_WORLD);
        }
    }
}


void initStructures(int source) {

    for (int i = id; i < N; i += nproc)
    {
        D[i] = G[source][i];
        visited[i] = false;
        pred[i] = source;
    }
    
    visited[source] = true;
    pred[source] = -1;
}



void dijkstra(int source)
{
    int lowestCost, lowestCostIndex;

    initStructures(source);
    
    for (int j = 1; j < N; j++)
    {

        // find vertex with lowest cost to visit
        // that was not visited before
        lowestCost = INF;
        for (int i = id; i < N; i += nproc)
        {
            if (!visited[i] && D[i] < lowestCost)
            {
                lowestCostIndex = i;
                lowestCost = D[i];
            }
        }

        // package found numbers to send them
        Vertex p, tmp_p;
        if (lowestCost < INF) {
            p.cost = lowestCost;
            p.index = lowestCostIndex;
        } else {
            p.cost = INF;
            p.index = INF;
        }

        MPI_Allreduce(&p, &tmp_p, 1, MPI_2INT, MPI_MINLOC, MPI_COMM_WORLD);

        lowestCostIndex = tmp_p.index;
        if (lowestCostIndex == INF) continue;
        D[lowestCostIndex] = tmp_p.cost;
        visited[lowestCostIndex] = true;

        // relax edge
        for (int i = id; i < N; i += nproc)
        {
            if (!visited[i] && D[i] > D[lowestCostIndex] + G[lowestCostIndex][i])
            {
                D[i] = D[lowestCostIndex] + G[lowestCostIndex][i];
                pred[i] = lowestCostIndex;
            }
        }
    }

}


void printResult(int source) {

    int sptr = 0;

    int S[SIZE];
    
    cout << endl << "Paths starting at " << source << endl;
    cout << "---------------------------" << endl;

    for(int i = 0; i < N; i++)
    {
        cout << i << ": ";

        if (i == source) {
            cout << "Start vertex" << endl;
            continue;
	    }

        if(D[i] == INF || D[i] == 0) {
            cout << "No path to vertex" << endl;
            continue;
        }
        // Ścieżkę przechodzimy od końca ku początkowi,
        // Zapisując na stosie kolejne wierzchołki

        for(int j = i; j > -1; j = pred[j]) S[sptr++] = j;

        // Oblicz koszt sciezki
        int cost = 0;
        int prev = source;
        int t = sptr;
        while(t) {
            t--;
            cost += G[prev][S[t]];
            prev = S[t];
        }

        if (cost >= INF) {
            cout << "No path to vertex" << endl;
            continue;           
        }

        // Wyświetlamy ścieżkę, pobierając wierzchołki ze stosu
        while(sptr) {
            sptr--;
            cout << S[sptr] << " ";
        }

        // Na końcu ścieżki wypisujemy jej koszt
        cout << "$" << cost << endl;
        
    }

}



int main(int argc, char **argv)
{
    double t1, t2;

    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &nproc); // get totalnodes
    MPI_Comm_rank(MPI_COMM_WORLD, &id);    // get mynode

    if (id == 0) readGraphMatrix();

    MPI_Bcast(&N, 1, MPI_INT, 0, MPI_COMM_WORLD );
    MPI_Bcast(&(G[0][0]), N*N, MPI_INT, 0, MPI_COMM_WORLD );

    // start counting time
    if (id == 0) t1 = MPI_Wtime();
    //call the algorithm for all nodes
    for (int start = 0; start < N; start++) {
        dijkstra(start);
        gatherPredecessors();
        if(id == 0) printResult(start);
    }

    // stop clock and print
    if (id == 0) {
        t2 = MPI_Wtime();
        cout << "\n\ntime elapsed: " << (t2 - t1) << endl;
    }

    MPI_Finalize();
}
