# Implementacja algorytmu dijkstry w mpi

Implementacja wzorowana na:
https://www.tankonyvtar.hu/en/tartalom/tamop412A/2011-0063_23_introduction_mpi/ar01s07.html

Ta implementacja zakładała bardzo proste podejście, które łątwo wytłumaczyć. Ale w master procesie kończyły tylko koszty przejścia ścieek, musiałam dopisać wysyłanie tablicy poprzedników, zeby moc odtworzyć ściezkę.

Nie było tez wczytywania macierzy, to jeszcze do lekkiej modyfikacji, patrz uwagi.

Ogolnie kod tez zrefaktorowalam, zeby sie latwiej czytalo i dodalam dokumentacje.

## Co trzeba mieć zainstalowane

1. gcc compiler
2. mpich https://www.mpich.org/downloads/

## Uzywanie

Wszystkie rzeczy, ktore trzeba zrobic sa zebrane w makefile, tak jak chcial gronek. Wszystko w konsoli:

### W pracowni
```
source /opt/nfs/config/source_mpich32.sh
make nodes
(or /opt/nfs/config/station_name_list.sh 101 116 > nodes)
make run_fis
```


### Kompilacja
```
make
```

### Wyczyszczenie plików wykonywalnych
```
make clean
```

### Odpalenie na jednym procesie
```
make run
```

### Odpalenie na wielu procesach
Domyślnie testuję to na 2, bo mam 2 rdzenie, mozna zmieniac liczbę (linia 23 w makefile)
```
make run_multi
```

### Clean, kompilacja, uruchomienie multi
```
make all
```


## Uwagi

- implementację dla ściezek od wszystkich węzłów do wszystkich uzyskamy odpalając funkcję z source będącym kadym z węzłów (zrobic to?)
- nie ma tutaj rozsyłania macierzy do wszystkich węzłów, tylko kady wczytuje to lokalnie. Mozemy to rozwiązać wskazując ściezkę z taurusa, albo rozesłać. Dokończę to tak, czy tak, ale juz mona zacząć pisać sprawko.
- program zwraca infiinity (9999) jesli sciezki nie ma